#!/usr/bin/env perl 
# 
# File:		ptop_pool0.pl
#
# Summary:	dump smartnic's pool0 from ptop to csv
#
# Author:         ultraman
# E-Mail:         *****@infoblox.com
# Org:            Infoblox Support
#
# Orig-Date:      05-April-2019 
# Last-Mod:       

use strict;
use warnings;
use 5.010;

open  my $ptop, "<", $ARGV[0]  or die "problem with $ARGV[0] : $!";

my $tarikh;

while ( <$ptop> ) {

	if ( /^TIME\s.+\s.+\s(\S+\s\S+)$/ ) {

		$tarikh = $1;

	} elsif ( /^SNIC\s+qps.+pool\s(\d+)/ ) {
	
		say "${tarikh}, $1";

	}

}
